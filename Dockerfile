FROM rocker/shiny

# trying 1

RUN apt-get -qq update && \
    apt-get install --no-install-recommends -y wget psmisc procps sudo \
        libcurl4-openssl-dev curl libxml2-dev nginx python python3-pip net-tools \
        lsb-release tcpdump unixodbc unixodbc-dev odbcinst odbc-postgresql \
        texlive-latex-base texlive-extra-utils texlive-fonts-recommended \
        texlive-latex-recommended libapparmor1 libedit2 libcurl4-openssl-dev libssl-dev zlib1g-dev && \
    pip3 install bioblend argparse

RUN mkdir -p /etc/services.d/nginx

COPY 4Dockerfile/service-nginx-start /etc/services.d/nginx/run
COPY 4Dockerfile/service-nginx-stop  /etc/services.d/nginx/finish
COPY 4Dockerfile/proxy.conf          /etc/nginx/sites-enabled/default

# ENV variables to replace conf file from Galaxy
ENV DEBUG=false \
    GALAXY_WEB_PORT=10000 \
    CORS_ORIGIN=none \
    DOCKER_PORT=none \
    API_KEY=none \
    HISTORY_ID=none \
    REMOTE_HOST=none \
    GALAXY_URL=none \
    RSTUDIO_FULL=1 \
    DISABLE_AUTH=true

WORKDIR /import/

# end trying 1

ADD . /srv/shiny-server/SighmaOAD
RUN mkdir -p /srv/samples
RUN mkdir -p /srv/samples/one
ADD ./samples/one /srv/shiny-server/One

RUN chmod -R 777 /srv/shiny-server/SighmaOAD

RUN apt-get update && apt-get install openssl

RUN install2.r RSQLite shinyjs ggplot2 openssl plotly DT RColorBrewer shinyWidgets

