ui_btUser <- actionButton("btUser",  label = "", icon = icon("user"), title = TITLE_ICON_USER)

generate_ui_user <- function(user){
  fluidRow(
    id = "fuser",
    column(
      width = 8,
      offset = 2,
      align="center",
      div(
        id = "duser",
        disabled(textInput("ulastname", "Nom", user$lastname)),
        disabled(textInput("ufirstname", "Prenom", user$firstname)),
        disabled(textInput("uemail", "Adresse electronique", user$email)),
        disabled(textInput("ujob", "Profession", user$job)),
        disabled(textAreaInput("uinterest", "Intérêt pour l'application", user$interest))
      )
    )
  )
}