ui_logged <- fluidRow(
  id = "flogged",
  column(
    width = 10,
    offset = 1,
    fluidRow(
      column(
        width = 4,
        offset = 2,
        actionButton("btExplo", class="button_mode", label = HTML(BUTTON_MODE_EXPLO))
      ),
      column(
        width = 4,
        actionButton("btCompar", class="button_mode", label = HTML(BUTTON_MODE_COMPARE))
      )
    )
  )
)

generate_picto <- function(v){
  tabPanel(
    title = tags$img(src = file.path(dir_image, DIR_PICTO, v[,FILE_DICTIONARY_COL_PICTO]), alt=v[,FILE_DICTIONARY_COL_FAMILY], title=v[,FILE_DICTIONARY_COL_FAMILY]),
    value = v[,FILE_DICTIONARY_COL_FAMILY]
  )
}

generate_ui_explo <- function(dataset){
  #scenars <- levels(dataset@data[,dataset@col_group])
  scenars <- getDicScenarioOptions(dataset@scenarios)
  vars <- getDicVarOptions(dataset@vars)
  params <- getDicParamOptions(dataset@params)
  vars_picto <- unique(dataset@vars[,c(FILE_DICTIONARY_COL_FAMILY, FILE_DICTIONARY_COL_PICTO)])
  vars_picto <- vars_picto[!apply(vars_picto == "", 1, all),]
  
  picto_all <- data.frame(FILE_DICTIONARY_PICTO_ALL, FILE_DICTIONARY_FAMILY_ALL, stringsAsFactors = FALSE)
  names(picto_all) <- c(FILE_DICTIONARY_COL_PICTO, FILE_DICTIONARY_COL_FAMILY)
  
  vars_picto <- rbind(picto_all, vars_picto)
  
  tabs_picto <- lapply(1:nrow(vars_picto), function(i){generate_picto(vars_picto[i,])})
  
  col_plot <- column(
    width = 8,
    # plotlyOutput("pplot"),
    plotOutput(outputId = "pplot", width = MODE_EXPLO_PLOT_WIDTH, height = MODE_EXPLO_PLOT_HEIGHT)
  )
  
  col_tab <- column(
    width = 4,
    class = "tsummary",
    do.call(tabsetPanel, c(tabs_picto, id="tpicto")),
    DTOutput("tsummary")
  )
  
  if(MODE_EXPLO_PLOT_FIRST){
    flayout <- fluidRow(col_plot, col_tab)
  }else{
    flayout <- fluidRow(col_tab, col_plot)
  }
    
  fluidRow(
    id = "fexplo",
    column(
      width = 10,
      offset = 1,
      fluidRow(
        class = "selects_mode mode-explo-menu",
        column(
          width = 3,
          selectInput("igroup",
                      label = MODE_EXPLO_SELECT_SCENARIO,
                      choices = scenars,
                      selected = scenars[[1]])
        ),
        column(
          width = 3,
          selectInput("ix",
                      label = MODE_EXPLO_SELECT_X,
                      choices = vars,
                      selected = vars[[1]])
        ),
        column(
          width = 3,
          selectInput("iy",
                      label = MODE_EXPLO_SELECT_Y,
                      choices = vars,
                      selected = vars[[2]])
        ),
        column(
          width = 3,
          selectInput("iparam",
                      label = MODE_EXPLO_SELECT_PARAMETER,
                      choices = params,
                      selected = params[[1]])
        )
      ),
      flayout
    )
  )
}

generate_sliderInput <- function(param){
  sliderInput(paste0("sicParam", param$id),
              label = param[,FILE_DICTIONARY_COL_FULL_NAME],
              min = param$min,
              max = param$max,
              value = c(param$mean - param$delta, param$mean + param$delta)
  )
}
      
generate_ui_compar <- function(dataset){
  scenars <- getDicScenarioOptions(dataset@scenarios)
  vars <- getDicVarOptions(dataset@vars)
  params <- c(Tous="all", getDicParamOptions(dataset@params))
  
  fluidRow(
    id = "fcompar",
    useShinyjs(),
    class = "selects_mode",
    column(
      width = 10,
      offset = 1,
      fluidRow(
        column(
          width = 3,
          class = "mode-compar-menu",
          selectizeInput("icgroups", 
                         label = MODE_COMPAR_SELECT_SCENARIO, 
                         choices = scenars,
                         selected = list(as.character(scenars[[1]]), as.character(scenars[[2]]), as.character(scenars[[3]])),
                         multiple = TRUE,
                         options = list(plugins = list('remove_button'))),
          selectInput("icx",
                      label = MODE_COMPAR_SELECT_X,
                      choices = vars,
                      selected = vars[[1]]),
          selectInput("icy",
                      label = MODE_COMPAR_SELECT_Y,
                      choices = vars,
                      selected = vars[[2]]),
          selectInput("icparam",
                      label = MODE_COMPAR_SELECT_PARAMETER,
                      choices = params,
                      selected = params[[2]]),
          lapply(1:nrow(dataset@params), function(i){generate_sliderInput(dataset@params[i,])})
          
        ),
        column(
          width = 9,
          # plotlyOutput("pplot"),
          plotOutput(outputId = "pcplot", width = MODE_COMPAR_PLOT_WIDTH, height = MODE_COMPAR_PLOT_HEIGHT),
          fluidRow(
            column(
              width = 6,
              plotOutput(outputId = "pcplotboxx", width = MODE_COMPAR_BXPLOT_WIDTH, height = MODE_COMPAR_BXPLOT_HEIGHT)
            ),
            column(
              width = 6,
              plotOutput(outputId = "pcplotboxy", width = MODE_COMPAR_BXPLOT_WIDTH, height = MODE_COMPAR_BXPLOT_HEIGHT)
            )
          )
        )
      )
    )
  )
}