ui_btLogout <- actionButton("btLogout", label = "", icon = icon("sign-out"), title=TITLE_ICON_LOGOUT)
ui_btHome <- actionButton("btHome", label = "", icon = icon("home"), title=TITLE_ICON_HOME)
ui_btHomeRedirect <- actionButton("btHomeRedirect", label = "", icon = icon("home"), title=TITLE_ICON_HOME, onclick=paste('window.location="', SITE_URL, '"', sep=""))
ui_btPrint <- actionButton("btPrint", label = "", icon = icon("print"), title=TITLE_ICON_PRINT)
ui_btHelp <- dropdownButton(
  inputId = "ddbtHelp",
  label = "",
  icon = icon("question"),
  status = "primary",
  circle = FALSE,
  tags$a(href = file.path(DIR_PDF, FILE_HELP_SIMU), target="_blank", TITLE_ICON_HELP_SIMU),
  tags$a(href = file.path(DIR_PDF, FILE_HELP_ALIM), target="_blank", TITLE_ICON_HELP_ALIM)
)

ui_tos <- fluidRow(
  id = "ftos",
  column(
    width = 10,
    offset = 1,
    includeHTML(file.path(dir_www, dir_html, FILE_TOS))
  )
)

ui_mentions <- fluidRow(
  id = "fmentions",
  column(
    width = 10,
    offset = 1,
    includeHTML(file.path(dir_www, dir_html, FILE_MENTIONS))
  )
)

ui_mentions_link <- actionButton("btMentionsLink", label = TITLE_ICON_MENTIONS)
ui_site_link <- tags$a(id = "btSiteLink", href = SITE_URL, target = "_blank", SITE_URL)

ui_footer <- fluidRow(
  id = "ffooter",
  column(
    width = 10,
    offset = 1,
    div(
      id = "dfooter",
      class = "footer",
      actionButton("btCopyright", label = HTML("<a href=\"http://www.inra.fr\" target=\"_blank\">&copy;INRA</a>")),
      actionButton("btTos", label = TITLE_ICON_TOS),
      actionButton("btMentions", label = TITLE_ICON_MENTIONS)
    )
  )
)

ui_footer2 <- fluidRow(
  id = "ffooter2",
  column(
    width = 12,
    offset = 0, 
    div(
      id = "dfooter2",
      class = "header",
      tags$img( src = file.path(dir_image, DIR_LOGO, DIR_LOGO_FINANCEURS, "01_INRA.jpg"), alt=""),
      tags$img( src = file.path(dir_image, DIR_LOGO, DIR_LOGO_FINANCEURS, "02_Agro Campus Ouest.jpg"), alt=""),
      tags$img( src = file.path(dir_image, DIR_LOGO, DIR_LOGO_FINANCEURS, "03_irstea.jpg"), alt=""),
      tags$img( src = file.path(dir_image, DIR_LOGO, DIR_LOGO_FINANCEURS, "04_Région Bretagne.jpg"), alt=""),
      tags$img( src = file.path(dir_image, DIR_LOGO, DIR_LOGO_FINANCEURS, "05_Région Normandie.jpg"), alt=""),
      tags$img( src = file.path(dir_image, DIR_LOGO, DIR_LOGO_FINANCEURS, "06_Région Nouvelle Aquitaine.jpg"), alt=""),
      tags$img( src = file.path(dir_image, DIR_LOGO, DIR_LOGO_FINANCEURS, "07_Région Pays de la Loire.jpg"), alt="")
    )
  )
)


ui_header <- fluidRow(
  id = "fheader",
  column(
    width = 12,
    offset = 0, 
    div(
      id = "dheader",
      class = "header",
      tags$img( src = file.path(dir_image, DIR_LOGO, "logo1.png"), alt=""),
      tags$img( src = file.path(dir_image, DIR_LOGO, "logo2.gif"), alt="" ),
      tags$img( src = file.path(dir_image, DIR_LOGO, "logo3.jpg"), alt="" ),
      tags$img( src = file.path(dir_image, DIR_LOGO, "logo4.png"), alt="" ),
      tags$img( src = file.path(dir_image, DIR_LOGO, "logo5.png"), alt="" ),
      tags$img( src = file.path(dir_image, DIR_LOGO, "logo6.png"), alt="" )
    )
  )
)

ui_header2 <- fluidRow(
  id = "fheader2",
  column(
    width = 12,
    offset = 0, 
    div(
      id = "dheader2",
      class = "header",
      tags$img( src = file.path(dir_image, DIR_LOGO, DIR_LOGO_CONCEPTEUR, "gauche_INRA.jpg"), alt=""),
      tags$img( src = file.path(dir_image, DIR_LOGO, DIR_LOGO_CONCEPTEUR, "droite_institut_elevage.jpg"), alt=""),
      tags$img( src = file.path(dir_image, DIR_LOGO, DIR_LOGO_CONCEPTEUR, "PSDR 4.jpg"), alt="")
    )
  )
)


ui_home_intro <- fluidRow(
  id = "fsiteIntro",
  column(
    width = 10,
    offset = 1,
    div(
    id = "dsiteIntro",
    class ="siteIntro",
    HTML(TEXT_HOME_MAIN_INTRO)
    )
  )
)

ui_home_intro2 <- fluidRow(
  id = "fsiteIntro2",
  column(
    width = 10,
    offset = 1,
    div(
      id = "dsiteIntro2",
      class ="siteIntro",
      HTML(TEXT_HOME_LOGGED_INTRO)
    )
  )
)

ui_home_title <- fluidRow(
  id = "fsiteTitle",
  column(
    width = 10,
    offset = 1, 
    div(
      id = "dsiteTitle",
      class = "siteTitle",
      HTML(TITLE_MAIN)
    )
  )
)

ui_menubar <- fluidRow(
  id = "fmenubar",
  column(
    width = 10,
    offset = 1, 
    id = "cmenubar",
    class = "cmenubar"
  )
)


ui_console <- fluidRow(
  id = "fconsole",
  column(
    width = 10,
    offset = 1,
    div(
      id = "dconsole",
      verbatimTextOutput("tconsole")
    )
  )
) 

ui_insertUI <- fluidRow(
  id = "finsertUI",
  column(
    width = 10,
    offset = 1,
    div(id = "dinsertUI")
  )
)

jsCode <- "shinyjs.winprint = function(){
  
  window.print();

}"

 
ui_home <- fluidPage(
  useShinyjs(),
  extendShinyjs(text = jsCode, functions = c("winprint")),
  class = "ui_home",
  tags$head(
    tags$link(rel = "stylesheet", type = "text/css", href = file.path(dir_css ,"sighma.css")),
    tags$link(rel = "stylesheet", type = "text/css", href = file.path(dir_css ,"sighma_print.css"), media = "print"),
    tags$title(TITLE_MAIN_SHORT)
  ),
  tags$head(tags$title(TITLE_MAIN_SHORT)),
  
  ui_header2,
  ui_home_title,
  ui_menubar,
  ui_console,
  ui_home_intro,
  ui_insertUI,
  ui_login,
  ui_footer,
  ui_footer2
  # tags$head(tags$script(src = file.path("js", "vendor", "plotly", "plotly-locale-fr-latest.js"))),
  # tags$head(tags$script("Plotly.setPlotConfig({locale: 'fr'})"))
)