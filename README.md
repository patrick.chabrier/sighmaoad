SIGHMA OAD
==========

## Utilisations de l'application SighmaOAD

Le mode démonstration est disponible en utilisant l'utilisateur _demo_ avec le mot de passe _demo_.

### avec RStudio

* télécharger ou cloner l'intégralité du projet
* lancer RStudio
* ouvrir le fichier _app.R_
* utiliser le bouton "Run App"

### avec Docker "almost everywhere"

#### sous Windows

* installer Docker en allant à cette adresse:[Install Docker Desktop on Windows](https://docs.docker.com/docker-for-windows/install/)
* lancer un terminal "invite de commandes"
* pour se connecter au registry de l'image Docker de SighmaOAD copier/coller la commande `docker login registry.forgemia.inra.fr` dans le terminal
* vous pouvez utiliser les identifiants (tant que le projet [SIGHMA OAD](https://forgemia.inra.fr/SIghmaOAD/sighmaoad) est privé) username: pchabrier Password: 9odyq-oAtaUZk5xskxZa
* copier coller la commande `docker run -p 3838:3838 registry.forgemia.inra.fr/sighmaoad/sighmaoad/sighmaoad` pout lancer le container Docker SighmaOAD.
* l'adresse "http://localhost:3838/SighmaOAD/" permet d'accéder à l'application web.
* en tant que membre du projet [SIGHMA OAD](https://forgemia.inra.fr/SIghmaOAD/sighmaoad) vous pouvez utilisez vos propres identifiants pour accéder au registry du projet. Le point d'entrée dans la documentation est là [Personal access tokens](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)

